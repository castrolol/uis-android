package cuzao.aprendendo.android.learningfrete

/**
 * Created by castrolol on 14/01/17.
 */


object OfertaRepository {

    val ofertas = listOf(
            Oferta(1, "Cuiaba, MT", "Fazenda Tanguro", "Campo Grande, MS", "Via de Dados", R.drawable.ic_nuvem,  8, 58, 43, 60 ),
            Oferta(2, "Cuiaba, MT", "Fazenda Pirarucu", "Porto Velho, RO", "Terminal de Porto Velho", R.drawable.ic_maggi,  36, 90, 40, 55 ),
            Oferta(3, "Varzea Grande, MT", "Fazenda Trabe", "Porto Velho, RO", "Porto da Esquerda", R.drawable.ic_ldc,  12, 52, 52, 62 ),
            Oferta(4,      "Barretos, GO", "Fazenda Sertaneja", "Manaus, AM", "Terminal de Zona Franca", R.drawable.ic_nuvem,  24, 110, 46, 55 ),
            Oferta(5,   "Querência, MT", "Fazenda Tanguro", "Porto Velho, RO", "Terminao de Porto Velho", R.drawable.ic_maggi,  36, 90, 40, 55 )


    )

    fun getAll()  = ofertas.toTypedArray()

    fun getOferta(id: Int) = ofertas.firstOrNull { it.id == id }

}