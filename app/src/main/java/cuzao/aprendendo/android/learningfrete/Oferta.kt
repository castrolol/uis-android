package cuzao.aprendendo.android.learningfrete

/**
 * Created by castrolol on 14/01/17.
 */

data class Oferta(
        val id: Int,
    val origem: String,
    val origemDetalhe: String,
    val destino: String,
    val destinoDetalhe: String,
    val logo: Int,
    val validade: Int,
    val valor: Int,
    val cargaMin: Int,
    val cargaMax: Int


)