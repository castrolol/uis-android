package cuzao.aprendendo.android.learningfrete

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        title = "Ofertas de Carga";

        initLista()



    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.ofertas_menu, menu)
        return true
    }

    fun initLista(){

        val lista = findViewById(R.id.oferta_list_view) as ListView


        val ofertaAdapter = OfertaListaAdapter(this, OfertaRepository.getAll())


        lista.adapter = ofertaAdapter

        lista.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->


            showOferta(id)

        }




    }

    private fun showOferta(id: Long) {

        val intent = Intent(this, OfertaDetalheActivity::class.java)
        intent.putExtra("id", id)

        startActivity(intent)

    }
}
