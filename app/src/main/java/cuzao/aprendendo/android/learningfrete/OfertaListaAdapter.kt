package cuzao.aprendendo.android.learningfrete

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by castrolol on 14/01/17.
 */

class OfertaListaAdapter(context: Context, objects: Array<Oferta>) : ArrayAdapter<Oferta>(context, R.layout.oferta_item, R.id.tv_origem, objects) {

    override fun getItemId(position: Int): Long {
        return (super.getItem(position).id).toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = super.getView(position, convertView, parent)
        val item = getItem(position)

        val origemText = v.findViewById(R.id.tv_origem) as TextView
        val origemDetalheText = v.findViewById(R.id.tv_origem_detalhe) as TextView
        val destinoText = v.findViewById(R.id.tv_destino) as TextView
        val destinoDetalheText = v.findViewById(R.id.tv_destino_detalhe) as TextView
        val logoImage = v.findViewById(R.id.iv_logo) as ImageView
        val cargaText = v.findViewById(R.id.tv_carga) as TextView
        val valorText = v.findViewById(R.id.tv_valor) as TextView
        val validadeText = v.findViewById(R.id.tv_validade) as TextView

        origemText.text = item.origem
        origemDetalheText.text = item.origemDetalhe
        destinoText.text = item.destino
        destinoDetalheText.text = item.destinoDetalhe
        logoImage.setImageResource(item.logo)
        cargaText.text = "${item.cargaMin} - ${item.cargaMax}"
        valorText.text = "${item.valor} / Ton"
        validadeText.text = "${item.validade} H"



        return v
    }
}
