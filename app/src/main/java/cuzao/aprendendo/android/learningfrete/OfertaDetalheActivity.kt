package cuzao.aprendendo.android.learningfrete

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class OfertaDetalheActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_oferta_detalhe_activity)


        val id = intent.extras.getLong("id").toInt()
        val oferta = OfertaRepository.getOferta(id)

        init(oferta)

    }

    private fun init(item: Oferta?) {

        if(item == null) return


        val v = this
        val origemText = v.findViewById(R.id.tv_origem) as TextView
        val origemDetalheText = v.findViewById(R.id.tv_origem_detalhe) as TextView
        val destinoText = v.findViewById(R.id.tv_destino) as TextView
        val destinoDetalheText = v.findViewById(R.id.tv_destino_detalhe) as TextView
        val logoImage = v.findViewById(R.id.iv_logo) as ImageView

        origemText.text = item.origem
        origemDetalheText.text = item.origemDetalhe
        destinoText.text = item.destino
        destinoDetalheText.text = item.destinoDetalhe
        logoImage.setImageResource(item.logo)


    }
}
